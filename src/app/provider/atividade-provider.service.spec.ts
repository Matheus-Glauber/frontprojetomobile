import { TestBed } from '@angular/core/testing';

import { AtividadeProviderService } from './atividade-provider.service';

describe('AtividadeProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AtividadeProviderService = TestBed.get(AtividadeProviderService);
    expect(service).toBeTruthy();
  });
});
