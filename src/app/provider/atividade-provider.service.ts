import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { resolve, reject } from 'q';

@Injectable({
  providedIn: 'root'
})
export class AtividadeProviderService {

  private apiUrl = "https://http://localhost:8080/"

  constructor(public http: HttpClient) {}

  post(nomeAtividade: String, descricaoAtividade: String){
    return new Promise((resolve, reject) => {
      var data = {
        nomeAtividade: nomeAtividade,
        descricaoAtividade: descricaoAtividade
      };

      this.http.post(this.apiUrl + 'atividade/', data).subscribe((result: any) =>{
        resolve(result.json());
      },
      (error) =>{
        reject(error.json());
      });
    }); 
  }

  getAll(page: number){
    return new Promise((resolve, reject) =>{
      let url = this.apiUrl + "atividade/?per_page=10&page=" + page;

      this.http.get(url).subscribe((result: any) =>{
        resolve(result.json());
      },
      (error) => {
        reject(error.json());
      });
    });
  }

  get(id: number){
    return new Promise((resolve, reject) =>{
      let url = this.apiUrl + "atividade/" + id;

      this.http.get(url).subscribe((result: any) =>{
        resolve(result.json());
      },
      (error) =>{
        reject(error.json());
      });
    });
  }

  insert(atividade: any){
    return new Promise((resolve, reject) =>{
      let url = this.apiUrl + "atividade/";
      
      this.http.post(url, atividade).subscribe((result: any) =>{
        resolve(result.json());
      },
      (error) => {
        reject(error.json());
      });
    });
  }

  update(atividade: any){
    return new Promise((resolve, reject) =>{
      let url = this.apiUrl + "atividade/" + atividade.id;
      let data = {
        "nomeAtividade" : atividade.nomeAtividade,
        "descricaoAtividade" : atividade.descricaoAtividade
      }

      this.http.put(url, atividade).subscribe((result: any) =>{
        resolve(result.json());
      },
      (error) => {
        reject(error.json());
      });
    });
  }

  remove(id: number){
    return new Promise((resolve, reject) =>{
      let url = this.apiUrl + "atividade/" + id;

      this.http.delete(url).subscribe((result: any) =>{
        resolve(result.json());
      },
      (error) =>{
        reject(error.json());
      });
    });
  }
}
